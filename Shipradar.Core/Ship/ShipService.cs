﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shipradar.Core.Ship
{
    public class ShipService : IShipService
    {
        private readonly IList<string> availableShips;

        public ShipService()
        {
            this.availableShips = new List<string>
            {
                "aidaaura",
                "aidacara",
                "aidavita",
                "aidablu",
                "aidabella",
                "aidasol",
                "aidamar",
                "aidaluna",
                "aidadiva",
                "aidastella",
                "aidaprima"
            };
        } 
        public IEnumerable<Ship> GetShips()
        {
            return this.availableShips.Select(ship => new Ship {Name = ship}).ToList();
        }

        public Ship GetShip(string name)
        {
            if (!this.availableShips.Contains(name.ToLower()))
            {
                throw new ArgumentException("Uknown ship name.");
            }

            return new Ship {Name = name };
        }
    }
}