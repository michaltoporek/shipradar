﻿using System.Collections.Generic;

namespace Shipradar.Core.Ship
{
    public interface IShipService
    {
        IEnumerable<Ship> GetShips();

        Ship GetShip(string name);
    }
}
