﻿namespace Shipradar.Core.Position
{
    public class Position
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Speed { get; set; }

        public double Temperature { get; set; }

        public double WindSpeed { get; set; }

        public double Course { get; set; }
    }
}
