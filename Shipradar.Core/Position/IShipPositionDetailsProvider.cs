﻿namespace Shipradar.Core.Position
{
    public interface IShipPositionDetailsProvider
    {
        Position GetShipPosition(string shipName);
    }
}
