﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace Shipradar.Core.Position
{
    public class KreuzFahrtBeraterProvider : IShipPositionDetailsProvider
    {
        private const string WebPageBaseAddress = "http://www.kreuzfahrtberater.de/schiffsposition.php";

        private readonly IDictionary<string, string> shipsDictionary;

        public KreuzFahrtBeraterProvider()
        {
            this.shipsDictionary = GetDictionaryWithShips();
        }

        public Position GetShipPosition(string shipName)
        {
            if (this.shipsDictionary.ContainsKey(shipName.ToLower()))
            {
                string url = this.shipsDictionary[shipName.ToLower()];
                string html = GetHtmlString(url);

                Position position = new Position();
                position.Latitude = GetLatitude(html);
                position.Longitude = GetLongitude(html);
                position.Speed = GetSpeed(html);
                position.Temperature = GetTemperature(html);
                position.WindSpeed = GetWindSpeed(html);
                position.Course = GetCourse(html);

                return position;
            }

            throw new ArgumentException("Provided ship name doesn't exists.");
        }

        private IDictionary<string, string> GetDictionaryWithShips()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("aidastella", GetShipAddress("?schiff=AIDA+stella"));
            dictionary.Add("aidaaura", GetShipAddress("?schiff=AIDA+aura"));
            dictionary.Add("aidacara", GetShipAddress("?schiff=AIDA+cara"));
            dictionary.Add("aidavita", GetShipAddress("?schiff=AIDA+vita"));
            dictionary.Add("aidabella", GetShipAddress("?schiff=AIDA+bella"));
            dictionary.Add("aidablu", GetShipAddress("?schiff=AIDA+blu"));
            dictionary.Add("aidaluna", GetShipAddress("?schiff=AIDA+luna"));
            dictionary.Add("aidamar", GetShipAddress("?schiff=AIDA+mar"));
            dictionary.Add("aidaprima", GetShipAddress("?schiff=AIDA+prima"));
            dictionary.Add("aidasol", GetShipAddress("?schiff=AIDA+sol"));
            dictionary.Add("aidadiva", GetShipAddress("?schiff=AIDA+diva"));

            return dictionary;
        }

        private string GetShipAddress(string shipUrl)
        {
            return string.Concat(WebPageBaseAddress, shipUrl);
        }

        private string GetHtmlString(string url)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadString(url);
            }
        }

        private double GetLatitude(string html)
        {
            string longitudeStr = ExtractValueFromHtml(html, "cent_lat: ", ",");

            double longitude = 0;

            if (!string.IsNullOrEmpty(longitudeStr))
            {
                double.TryParse(longitudeStr, out longitude);
            }

            return longitude;
        }

        private double GetLongitude(string html)
        {
            string longitudeStr = ExtractValueFromHtml(html, "cent_lon: ", "");

            double longitude = 0;

            if (!string.IsNullOrEmpty(longitudeStr))
            {
                double.TryParse(longitudeStr, out longitude);
            }

            return longitude;
        }

        private double GetSpeed(string html)
        {
            string speedStr = ExtractValueFromHtml(html, "Geschwindigkeit: ", "kn");

            double speed = 0;

            if (!string.IsNullOrEmpty(speedStr))
            {
                double.TryParse(speedStr.Replace(',','.'),  out speed);
            }

            return speed;
        }

        private double GetWindSpeed(string html)
        {
            string windSpeedStr = ExtractValueFromHtml(html, "Windgeschwindigkeit: ", "m");

            double windSpeed = 0;

            if (!string.IsNullOrEmpty(windSpeedStr))
            {
                double.TryParse(windSpeedStr.Replace(',', '.'), out windSpeed);
            }

            return windSpeed;
        }

        private double GetTemperature(string html)
        {
            string temeratureStr =  ExtractValueFromHtml(html, "Temperatur: ", "&deg");

            double temperature = 0;

            if (!string.IsNullOrEmpty(temeratureStr))
            {
                double.TryParse(temeratureStr.Replace(',', '.'), out temperature);
            }

            return temperature;
        }

        private double GetCourse(string html)
        {
            string courseStr = ExtractValueFromHtml(html, "Kurs: ", "&deg;</li>");

            double course = 0;

            if (!string.IsNullOrEmpty(courseStr))
            {
                double.TryParse(courseStr, out course);
            }

            return course;
        }


        private string ExtractValueFromHtml(string html, string startText, string endText)
        {
            Regex rgx = new Regex($@"(?<={startText})(.*)(?={endText})");
            Match match = rgx.Match(html);
            double value = 0;

            if (match.Success)
            {
                return match.Value;
            }

            return string.Empty;
        }
    }
}