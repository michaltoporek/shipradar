﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Shipradar.Core.Ship;

namespace Shipradar.Api.Controllers
{
    public class ShipController : ApiController
    {
        private readonly IShipService shipService;

        public ShipController(IShipService shipService)
        {
            this.shipService = shipService;
        }

        [HttpGet]
        [Route("api/ships")]
        public IEnumerable<Ship> Get()
        {
            var ships = this.shipService.GetShips();

            return ships;
        }

        [HttpGet]
        [Route("api/ships/{name}")]
        public Ship Get(string name)
        {
            try
            {
                return this.shipService.GetShip(name);
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }

        }
    }
}
