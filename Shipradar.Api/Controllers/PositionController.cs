﻿using System.Web.Http;
using Shipradar.Core.Position;

namespace Shipradar.Api.Controllers
{
    public class PositionController : ApiController
    {
        private readonly IShipPositionDetailsProvider positionDetailsProvider;

        public PositionController(IShipPositionDetailsProvider positionDetailsProvider)
        {
            this.positionDetailsProvider = positionDetailsProvider;
        }

        [HttpGet]
        [Route ("api/ships/{name}/position")]
        public Position GetPosition(string name)
        {
            return this.positionDetailsProvider.GetShipPosition(name);
        }
    }
}
