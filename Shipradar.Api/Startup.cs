﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Shipradar.Api.Startup))]

namespace Shipradar.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
