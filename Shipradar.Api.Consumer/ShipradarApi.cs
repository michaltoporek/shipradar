﻿using System;
using System.Collections.Generic;
using RestSharp;
using Shipradar.Api.Consumer.Ship;

namespace Shipradar.Api.Consumer
{
    public class ShipradarApi
    {
        private const string BaseUrl = "http://shipradarapi.azurewebsites.net/api/";

        public T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient(BaseUrl);
            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }

            return response.Data;
        }


    }
}
