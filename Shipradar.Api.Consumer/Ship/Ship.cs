﻿using Shipradar.Api.Consumer.Ship.Position;

namespace Shipradar.Api.Consumer.Ship
{
    public class Ship
    {
        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public ShipPosition Position { get; set; }
    }
}
