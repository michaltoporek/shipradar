﻿namespace Shipradar.Api.Consumer.Ship.Position
{
    public class ShipPosition
    {
        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public double Course { get; set; }

        public double Speed { get; set; }

        public double Temperature { get; set; }

        public double WindSpeed { get; set; }
    }
}
