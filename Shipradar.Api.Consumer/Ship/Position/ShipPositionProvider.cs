﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Shipradar.Api.Consumer.Ship.Position
{
    public class ShipPositionProvider
    {
        private const string ShipResourceName = "Ships";
        private const string PositionResourceName = "Position";

        private readonly ShipradarApi shipradarApi;

        public ShipPositionProvider(ShipradarApi shipradarApi)
        {
            this.shipradarApi = shipradarApi;
        }

        public ShipPosition GetPositionOfShip(string shipName)
        {
            var request = new RestRequest
            {
                Resource = $"{ShipResourceName}/{shipName}/{PositionResourceName}",
                RootElement = ShipResourceName
            };

            return this.shipradarApi.Execute<ShipPosition>(request);
        }
    }
}
