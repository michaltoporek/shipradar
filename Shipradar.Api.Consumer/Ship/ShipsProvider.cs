﻿using System.Collections.Generic;
using RestSharp;

namespace Shipradar.Api.Consumer.Ship
{
    public class ShipsProvider
    {
        private const string ShipResourceName = "Ships";

        private readonly ShipradarApi shipradarApi;

        public ShipsProvider(ShipradarApi shipradarApi)
        {
            this.shipradarApi = shipradarApi;
        }

        public Ship GetShip(string shipName)
        {
            var request = new RestRequest
            {
                Resource = $"{ShipResourceName}/{shipName}",
                RootElement = ShipResourceName
            };

            return this.shipradarApi.Execute<Ship>(request);
        }

        public List<Consumer.Ship.Ship> GetShips()
        {
            var request = new RestRequest {Resource = ShipResourceName };

            return this.shipradarApi.Execute<List<Ship>>(request);
        }
    }
}
