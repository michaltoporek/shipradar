﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Shipradar.GoogleMaps.Default" %>

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Ships</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>

 <script>

// The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: 0, lng: 0}
  });

  setMarkers(map);
}


var ships = [
  ['AIDAaura', 15.8930659815, 73.6882510818, 1],
  ['AIDAcara', 21.68914178996, -19.8659312725, 2],
  ['AIDAvita', 23.4344716142, -73.3687376976, 3],
  ['AIDAblu', 32.642147, -16.911275, 4],
  ['AIDAbella', 5.8664024469, 104.1387820244, 5],
  ['AIDAsol', 30.3799977039, -16.1454772918, 6],
  ['AIDAmar', 19.265867, -81.31617, 7],
  ['AIDAluna', 18.412252, -77.10983, 8],
  ['AIDAstella', 24.528698, 54.380965, 9],

];

function setMarkers(map) {

    for (var i = 0; i < ships.length; i++) {
        var ship = ships[i];

    var infowindow = new google.maps.InfoWindow({
         content: 'AIDA'
     });

    var marker = new google.maps.Marker({
        position: { lat: ship[1], lng: ship[2] },
        map: map,
        title: ship[0],
        zIndex: ship[3]
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
  }
}

    </script>
      

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3I2I2RTZzbO4Bq3AWb3adoyiKelvtTRU&signed_in=true&callback=initMap"></script>
      
  </body>
</html>