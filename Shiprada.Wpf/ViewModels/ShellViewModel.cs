﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Caliburn.Micro;
using Shipradar.Api.Consumer.Ship;
using Shipradar.Api.Consumer.Ship.Position;

namespace Shiprada.Wpf.ViewModels
{
    public class ShellViewModel : Screen
    {
        private const int IntervalMinutes = 60;

        private readonly ShipsProvider shipsProvider;
        private readonly ShipPositionProvider shipPositionProvider;
        private readonly BackgroundWorker shipRefreshWorker;

        public ShellViewModel(ShipsProvider shipsProvider, ShipPositionProvider shipPositionProvider)
        {
            DisplayName = "Shipradar";

            this.shipsProvider = shipsProvider;
            this.shipPositionProvider = shipPositionProvider;
            this.shipRefreshWorker = new BackgroundWorker();
            this.shipRefreshWorker.WorkerSupportsCancellation = true;
            this.shipRefreshWorker.DoWork += ShipRefreshWorkerDoWork;

            Ships = this.shipsProvider.GetShips();

            foreach (Ship ship in Ships)
            {
                ship.IsSelected = true;
            }
        }

        public void RefreshPositions()
        {
            RefreshShipPositions();
        }

        private void ShipRefreshWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(TimeSpan.FromMinutes(IntervalMinutes));

            RefreshShipPositions();
        }

        private void RefreshShipPositions()
        {
            IEnumerable<Ship> selectedShips = Ships.Where(x => x.IsSelected);

            foreach (Ship ship in selectedShips)
            {
                ShipPosition position = this.shipPositionProvider.GetPositionOfShip(ship.Name);

                ship.Position = position;
            }
        }

        public IList<Ship> Ships { get; set; }

        protected override void OnActivate()
        {
            base.OnActivate();
        }
    }
}
