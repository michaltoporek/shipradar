﻿using Autofac;
using Caliburn.Micro;
using Shipradar.Api.Consumer;
using Shipradar.Api.Consumer.Ship;
using Shipradar.Api.Consumer.Ship.Position;

namespace Shiprada.Wpf.DependencyInjection.Modules
{
    internal class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WindowManager>().As<IWindowManager>();

            builder.RegisterType<ShipsProvider>().AsSelf();
            builder.RegisterType<ShipPositionProvider>().AsSelf();
            builder.RegisterType<ShipradarApi>().AsSelf();
        }
    }
}
