﻿using Autofac;
using Shiprada.Wpf.DependencyInjection.Modules;

namespace Shiprada.Wpf.DependencyInjection
{
    public class DependencyManager
    {
        public static IContainer Container { get; private set; }

        public static void RegisterModules(ContainerBuilder builder)
        {
            builder.RegisterModule<ApplicationModule>();
        }

        public static void SetContainer(IContainer container)
        {
            Container = container;
        }
    }
}
