﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using Caliburn.Micro.Autofac;
using Shiprada.Wpf.DependencyInjection;
using Shiprada.Wpf.ViewModels;

namespace Shiprada.Wpf
{
    public class AppBootstraper : AutofacBootstrapper<ShellViewModel>
    {
        public AppBootstraper()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            PreventMultipleStartUp();

            // Get container from AutofacBootstrapper to have access to it from application.
            DependencyManager.SetContainer(base.Container);

            // Show main window.
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void ConfigureBootstrapper()
        {
            base.ConfigureBootstrapper();

            // No need to pass event aggregator to view models. 
            // Only implement IHandle<>.
            AutoSubscribeEventAggegatorHandlers = true;
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            DependencyManager.RegisterModules(builder);
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            base.OnExit(sender, e);
        }

        private void PreventMultipleStartUp()
        {
            string currentProcessName = Process.GetCurrentProcess().ProcessName;

            if (Process.GetProcesses().Count(p => p.ProcessName == currentProcessName) > 1)
            {
                Application.Shutdown();
            }
        }

        #region Unhandled Exception Catch

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show(
                "Some unhandled error occurred. Check the log file.",
                "Error");
#endif
        }
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show(
                "Some unhandled error occurred. Check the log file.",
                "Error");
#endif
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show(
                "Some unhandled error occurred. Check the log file.",
                "Error");
#endif
        }

        #endregion
    }
}
